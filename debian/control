Source: ukopp
Section: admin
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
 pkg-config,
 libgtk-3-dev,
 libglib2.0-dev
Standards-Version: 4.6.2
Homepage: http://www.kornelix.com/ukopp.html
Vcs-Git: https://salsa.debian.org/debian/ukopp.git
Vcs-Browser: https://salsa.debian.org/debian/ukopp

Package: ukopp
Architecture: any
Depends: ${shlibs:Depends},
 udev [linux-any],
 ${misc:Depends}
Description: Full and incremental backup to disk or disk-like device
 Ukopp is used to copy or back-up disk files to a disk or disk-like device,
 such as a USB stick. It copies only new or modified files since the last
 backup, and is therefore quite fast. A GUI is used to navigate the file
 system to include or exclude files or directories at any level. These
 choices can be saved in a job file for repeated use. New files appearing
 within the included directories are handled automatically. Optionally,
 previous versions of the backup files can be retained instead of being
 overwritten. Files can be selectively restored using a GUI. Ownership
 and permissions are also restored, even if the target device uses a
 Microsoft file system.
