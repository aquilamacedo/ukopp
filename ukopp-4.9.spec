#  RPM spec file for ukopp

Name:          ukopp
Version:       4.9
Release:       1
Summary:       backup with retention of file versions
Group:         utils
Vendor:        kornelix
Packager:      kornelix@posteo.de
License:       GPL3
Source:        %{name}-%{version}.tar.gz
URL:           http://kornelix.com

%description
Copy files to backup media. 
Copies only new and modified files and is therefore quite fast. 
Specify directories or files to include or exclude at any level. 
Report disk/backup differences at summary, directory or file level.
Optionally retain prior file versions for a specified time or 
version count. Optionally verify backup files.

%prep
%setup -q

%build
make

%install
make install PREFIX=$RPM_BUILD_ROOT/usr

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/usr/bin/%{name} 
/usr/share/%{name}
/usr/share/doc/%{name}
/usr/share/applications/%{name}.desktop
/usr/share/man/man1/%{name}.1.gz


